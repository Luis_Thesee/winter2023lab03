public class Toaster{
		
	String brand;
	int watts;
	int slices;
	
	public void howManySlices(){
		System.out.println("this toaster can toast up to " + this.slices + " at the same time!");
	}
	
	public void power(){
		if (this.watts > 50000){
			System.out.println("this toaster will overheat!");
		}
		else {
			System.out.println("this toaster is fine, it wont overheat!");
		}
	}
}	


/* System.out.println( */